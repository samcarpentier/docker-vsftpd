#!/bin/sh

if [ -z $FTP_USER ]; then
  >&2 echo "ERROR: Variable FTP_USER not provided."
  exit 1
fi
if [ -z $FTP_PASS ]; then
  >&2 echo "ERROR: Variable FTP_USER not provided."
  exit 1
fi

addgroup -g 433 -S $FTP_USER
adduser -u 431 -D -G $FTP_USER -h /home/$FTP_USER -s /bin/false  $FTP_USER
echo "$FTP_USER:$FTP_PASS" | /usr/sbin/chpasswd
chown $FTP_USER:$FTP_USER /home/$FTP_USER/ -R
