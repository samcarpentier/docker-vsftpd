#!/bin/sh

if [ -z $SKIP_FTP_USER_CREATION ]; then
  /bin/sh /usr/local/bin/setup-ftp-user.sh
fi

CONFIG_FILE=/etc/vsftpd/vsftpd.conf

change_config() {
  K=$1
  V=$2
  grep -s -q "^$K=" $CONFIG_FILE && sed -i "s/^$K=.*/$K=$V/" $CONFIG_FILE || echo "$K=$V" >> $CONFIG_FILE
}

for config in $(env | grep '^VSFTPD_'); do
  ENV_K=${config%%=*}
  ENV_K=${ENV_K#VSFTPD_}
  ENV_K=$(echo $ENV_K | awk '{print tolower($0)}')
  change_config ${ENV_K} ${config#*=}
done

/usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf
