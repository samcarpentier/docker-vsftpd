# docker-vsftpd

## Run with Docker

```bash
docker run \
  -e FTP_USER=test \
  -e FTP_PASS=test \
  -e VSFTPD_PASV_MIN_PORT=21100 \
  -e VSFTPD_PASV_MAX_PORT=21110 \
  -e VSFTPD_PASV_ADDRESS=127.0.0.1 \
  samcarpentier/vsftpd:alpine
```
